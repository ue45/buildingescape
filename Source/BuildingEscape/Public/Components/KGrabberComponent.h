// Copyright (C) 2021 Oleksandr Manenko. All rights reserved.

#pragma once

#include "Components/ActorComponent.h"
#include "Components/InputComponent.h"
#include "CoreMinimal.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"

#include "KGrabberComponent.generated.h"

UCLASS( ClassGroup = ( Custom ), meta = ( BlueprintSpawnableComponent ) )
class BUILDINGESCAPE_API UKGrabberComponent: public UActorComponent
{
    GENERATED_BODY()

public:
    UKGrabberComponent();

protected:
    void BeginPlay() override;

public:
    void TickComponent(
        float                        DeltaTime,
        ELevelTick                   TickType,
        FActorComponentTickFunction *ThisTickFunction ) override;

private:
    UPROPERTY( EditAnywhere )
    float ReachDistance = 100.f;

    UPROPERTY()
    UPhysicsHandleComponent *GrabHandle = nullptr;

    UPROPERTY()
    UInputComponent *Input = nullptr;

private:
    void    SetupInput();
    void    Grab();
    void    Release();
    void    GetPlayerViewPoint( FVector *Location, FRotator *Rotation ) const;
    FVector GetPlayerReach() const;
    FHitResult GetFirstPhysicsBodyInReach() const;
};
