// Copyright (C) 2021 Oleksandr Manenko. All rights reserved.

#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"

#include "KOpenDoorComponent.generated.h"

class ATriggerVolume;
class UAudioComponent;

UCLASS( ClassGroup = ( Custom ), meta = ( BlueprintSpawnableComponent ) )
class BUILDINGESCAPE_API UKOpenDoorComponent: public UActorComponent
{
    GENERATED_BODY()

public:
    UKOpenDoorComponent();

protected:
    void BeginPlay() override;

public:
    void TickComponent(
        float                        DeltaTime,
        ELevelTick                   TickType,
        FActorComponentTickFunction *ThisTickFunction ) override;

private:
    void  OpenDoor( float DeltaTime );
    void  CloseDoor( float DeltaTime );
    void  RotateDoor( float DeltaTime, float Yaw, float Speed );
    float GetTotalMassOfActors() const;

private:
    UPROPERTY( EditAnywhere )
    AActor *Activator{};

    UPROPERTY( EditAnywhere )
    ATriggerVolume *PressurePlate{};

    UPROPERTY( EditAnywhere )
    float OpenAngle = 90.f;

    UPROPERTY( EditAnywhere )
    float DoorCloseDelay = .5f;

    UPROPERTY( EditAnywhere )
    float DoorOpenSpeed = .5f;

    UPROPERTY( EditAnywhere )
    float DoorCloseSpeed = 1.f;

    UPROPERTY( EditAnywhere )
    float PressurePlateActivationMass = 14.f;

    UPROPERTY()
    UAudioComponent *Audio = nullptr;

    bool bOpenDoorSound  = false;
    bool bCloseDoorSound = true;

    float DoorLastOpened = 0.f;
    float InitialYaw     = 0.f;
};
