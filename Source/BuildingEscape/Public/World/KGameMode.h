// Copyright (C) 2021 Oleksandr Manenko. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"

#include "KGameMode.generated.h"

UCLASS()
class BUILDINGESCAPE_API AKGameMode: public AGameModeBase
{
    GENERATED_BODY()
};
