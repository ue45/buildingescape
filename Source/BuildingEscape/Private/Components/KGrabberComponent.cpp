// Copyright (C) 2021 Oleksandr Manenko. All rights reserved.

#include "Components/KGrabberComponent.h"

#include "DrawDebugHelpers.h"
#include "Engine/Engine.h"
#include "Engine/World.h"
#include "GameFramework/PlayerController.h"
#include <Components/KGrabberComponent.h>

template <typename T>
T *SafeGetComponentByClass( UActorComponent *Component )
{
    T *FoundComponent = Component->GetOwner()->FindComponentByClass<T>();
    if ( !FoundComponent ) {
        UE_LOG(
            LogTemp,
            Error,
            TEXT( "There is no '%s' attached to '%s' which is required by its "
                  "'%s'." ),
            *T::StaticClass()->GetName(),
            *Component->GetOwner()->GetName(),
            *Component->GetClass()->GetName() );
    }

    return FoundComponent;
}

UKGrabberComponent::UKGrabberComponent()
{
    PrimaryComponentTick.bCanEverTick = true;
}

void UKGrabberComponent::BeginPlay()
{
    Super::BeginPlay();

    GrabHandle = SafeGetComponentByClass<UPhysicsHandleComponent>( this );
    Input      = SafeGetComponentByClass<UInputComponent>( this );

    SetupInput();
}

void UKGrabberComponent::TickComponent(
    float                        DeltaTime,
    ELevelTick                   TickType,
    FActorComponentTickFunction *ThisTickFunction )
{
    Super::TickComponent( DeltaTime, TickType, ThisTickFunction );

    if ( GrabHandle && GrabHandle->GrabbedComponent ) {
        GrabHandle->SetTargetLocation( GetPlayerReach() );
    }
}

void UKGrabberComponent::SetupInput()
{
    const FName GrabActionName = "Grab";

    Input->BindAction(
        GrabActionName,
        IE_Pressed,
        this,
        &UKGrabberComponent::Grab );
    Input->BindAction(
        GrabActionName,
        IE_Released,
        this,
        &UKGrabberComponent::Release );
}

void UKGrabberComponent::Grab()
{
    if ( !GrabHandle ) {
        return;
    }

    FHitResult HitResult = GetFirstPhysicsBodyInReach();
    if ( HitResult.GetActor() ) {
        GrabHandle->GrabComponentAtLocation(
            HitResult.GetComponent(),
            NAME_None,
            GetPlayerReach() );
    }
}

void UKGrabberComponent::Release()
{
    if ( GrabHandle && GrabHandle->GrabbedComponent ) {
        GrabHandle->ReleaseComponent();
    }
}

FHitResult UKGrabberComponent::GetFirstPhysicsBodyInReach() const
{
    FVector PlayerLocation;
    GetPlayerViewPoint( &PlayerLocation, nullptr );

    FHitResult HitResult;
    GetWorld()->LineTraceSingleByObjectType(
        HitResult,
        PlayerLocation,
        GetPlayerReach(),
        FCollisionObjectQueryParams{ ECollisionChannel::ECC_PhysicsBody },
        FCollisionQueryParams{ FName{}, false, GetOwner() } );

    return HitResult;
}

FVector UKGrabberComponent::GetPlayerReach() const
{
    FVector  PlayerLocation;
    FRotator PlayerRotation;
    GetPlayerViewPoint( &PlayerLocation, &PlayerRotation );

    return PlayerLocation + PlayerRotation.Vector() * ReachDistance;
}

void UKGrabberComponent::GetPlayerViewPoint(
    FVector * Location,
    FRotator *Rotation ) const
{
    if ( !Location && !Rotation ) {
        return;
    }

    FVector  PlayerLocation;
    FRotator PlayerRotation;
    GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(
        PlayerLocation,
        PlayerRotation );

    if ( Location ) {
        *Location = PlayerLocation;
    }

    if ( Rotation ) {
        *Rotation = PlayerRotation;
    }
}
