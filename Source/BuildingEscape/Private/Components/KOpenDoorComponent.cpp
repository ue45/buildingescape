// Copyright (C) 2021 Oleksandr Manenko. All rights reserved.

#include "Components/KOpenDoorComponent.h"

#include "Components/AudioComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/TriggerVolume.h"
#include "Engine/World.h"
#include "GameFramework/Actor.h"
#include "GameFramework/PlayerController.h"

UKOpenDoorComponent::UKOpenDoorComponent()
{
    PrimaryComponentTick.bCanEverTick = true;
}

void UKOpenDoorComponent::BeginPlay()
{
    Super::BeginPlay();

    InitialYaw = GetOwner()->GetActorRotation().Yaw;

    if ( !PressurePlate ) {
        UE_LOG(
            LogTemp,
            Error,
            TEXT( "'%s' has UKOpenDoorComponent but no PressurePlate set" ),
            *GetOwner()->GetName() );
    }

    Activator = GetWorld()->GetFirstPlayerController()->GetPawn();

    Audio = GetOwner()->FindComponentByClass<UAudioComponent>();
    if ( !Audio ) {
        UE_LOG(
            LogTemp,
            Error,
            TEXT( "'%s' has UKOpenDoorComponent but no UAudioComponent set" ),
            *GetOwner()->GetName() );
    }
}

void UKOpenDoorComponent::TickComponent(
    float                        DeltaTime,
    ELevelTick                   TickType,
    FActorComponentTickFunction *ThisTickFunction )
{
    Super::TickComponent( DeltaTime, TickType, ThisTickFunction );

    if ( !PressurePlate ) {
        return;
    }

    if ( GetTotalMassOfActors() >= PressurePlateActivationMass ) {
        OpenDoor( DeltaTime );
        DoorLastOpened = GetWorld()->GetTimeSeconds();
    } else {
        float CurrentTime = GetWorld()->GetTimeSeconds();
        if ( CurrentTime - DoorLastOpened > DoorCloseDelay ) {
            CloseDoor( DeltaTime );
        }
    }
}

void UKOpenDoorComponent::OpenDoor( float DeltaTime )
{
    RotateDoor( DeltaTime, OpenAngle + InitialYaw, DoorOpenSpeed );

    bCloseDoorSound = false;
    if ( Audio && !bOpenDoorSound ) {
        Audio->Play();
        bOpenDoorSound = true;
    }
}

void UKOpenDoorComponent::CloseDoor( float DeltaTime )
{
    RotateDoor( DeltaTime, InitialYaw, DoorCloseSpeed );

    bOpenDoorSound = false;
    if ( Audio && !bCloseDoorSound ) {
        Audio->Play();
        bCloseDoorSound = true;
    }
}

void UKOpenDoorComponent::RotateDoor( float DeltaTime, float Yaw, float Speed )
{
    FRotator Rotation = GetOwner()->GetActorRotation();
    Rotation.Yaw      = FMath::FInterpTo( Rotation.Yaw, Yaw, DeltaTime, Speed );
    GetOwner()->SetActorRotation( Rotation );
}

float UKOpenDoorComponent::GetTotalMassOfActors() const
{
    float TotalMass = 0.f;

    TArray<AActor *> OverlappingActors;
    PressurePlate->GetOverlappingActors( OverlappingActors );
    for ( AActor *Actor: OverlappingActors ) {
        auto Component = Cast<UPrimitiveComponent>( Actor->GetRootComponent() );
        if ( Component ) {
            TotalMass += Component->GetMass();
        }
    }

    return TotalMass;
}
