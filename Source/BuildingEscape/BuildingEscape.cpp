// Copyright (C) 2021 Oleksandr Manenko. All rights reserved.

#include "BuildingEscape.h"

#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE(
    FDefaultGameModuleImpl,
    BuildingEscape,
    "BuildingEscape" );
