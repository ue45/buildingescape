// Copyright (C) 2021 Oleksandr Manenko. All rights reserved.

using UnrealBuildTool;

public class BuildingEscape : ModuleRules
{
	public BuildingEscape(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore" });
		PrivateDependencyModuleNames.AddRange(new string[] {  });
	}
}
