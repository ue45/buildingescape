// Copyright (C) 2021 Oleksandr Manenko. All rights reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class BuildingEscapeTarget : TargetRules
{
	public BuildingEscapeTarget( TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.AddRange( new string[] { "BuildingEscape" } );
	}
}
